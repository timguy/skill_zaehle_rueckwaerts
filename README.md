module.exports = {
  labels: ["dependencies"],
  hostRules: [
    {
      hostType: 'maven',
      matchHost: 'gitlab.com',
      token: process.env.RENOVATE_TOKEN
    }
  ],
  includeForks: true
};
