package github.timguy.skill;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.speechlet.SpeechletV2;

import github.timguy.skill.helper.AlexaSpeechletResponse;

public class CountSpeechlet implements SpeechletV2 {
	private static final String REPROMPT_STANDARD = "Ne Zahl?";
	private static final String SPEECH_HELP_TEXT = "Du kannst einfach nur rückwärts zählen lassen. Nenn mir ne Zahl.";
	private static final String SSML_BREAK = "<break time=\"0.5s\" />";
	private static final Logger log = LoggerFactory
			.getLogger(CountSpeechlet.class);
	
    private static final String SESSION_LAST_INTENT_LIST= "SESSION_LAST_INTENT_LIST";
    private static final String SESSION_LAST_ASKED_NEWS_NUMBER = "SESSION_LAST_ASKED_NEWS_NUMBER";

    private static final String SPEECHCON_NA = "<say-as interpret-as=\"interjection\">na?</say-as>";
    private static final String SPEECHCON_HEILIGER_STROHSACK = "<say-as interpret-as=\"interjection\">heiliger strohsack</say-as>";

    private static final String SPEECHCON_BIS_BALD = "<say-as interpret-as=\"interjection\">bis bald</say-as>";
    private static final String SPEECHCON_TSCHOE = "<say-as interpret-as=\"interjection\">tschö</say-as>";
    private static final String PUNKT_ENDE = ". ";
    
	@Override
	public void onSessionStarted(
			SpeechletRequestEnvelope<SessionStartedRequest> requestEnvelope) {
		String requestId = requestEnvelope.getRequest().getRequestId();
		if (!isMonitoringRequest(requestId)) {
			log.debug("onSessionStarted requestId={}, sessionId={}",
					requestId,
					requestEnvelope.getSession().getSessionId());
		}
	}

	@Override
	public SpeechletResponse onLaunch(
			SpeechletRequestEnvelope<LaunchRequest> requestEnvelope) {
		
		String requestId = requestEnvelope.getRequest().getRequestId();
		if(isMonitoringRequest(requestId)) {
			log.debug("Monitoring");
			return null;
		}
		log.debug("onLaunch requestId={}, sessionId={}",
				requestId,
				requestEnvelope.getSession().getSessionId());
		return handleHelp();
	}

	private boolean isMonitoringRequest(String requestId) {
		return requestId.contains("Monitoring");
	}

    private void attachIntentToSession(String intentName, Session session) {
        Object lastIntents = session.getAttribute(SESSION_LAST_INTENT_LIST);
        if (lastIntents == null) {
            session.setAttribute(SESSION_LAST_INTENT_LIST, Arrays.asList(intentName));
        } else {
            @SuppressWarnings("unchecked")
			List<String> lastIntentList = (List<String>) lastIntents;
            lastIntentList.add(intentName);
            session.setAttribute(SESSION_LAST_INTENT_LIST, lastIntentList);
            log.info("LastIntents: " + String.join(", ", lastIntentList));
        }
    }
    
	@Override
	public SpeechletResponse onIntent(
			SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {
		Intent intent = requestEnvelope.getRequest().getIntent();
		String intentName = intent.getName();
		Session session = requestEnvelope.getSession();
		log.info("onIntent={} requestId={}, sessionId={}",
				intentName,
				requestEnvelope.getRequest().getRequestId(),
				session.getSessionId());

		attachIntentToSession(intentName, session);
		switch (intentName) {
			case "CountBackwardIntent" :
				return handleCountBackwardIntent(intent);
			case "AMAZON.HelpIntent" :
				return handleHelp();
			case "AMAZON.StopIntent" :
				return AlexaSpeechletResponse.tell()
						.withText("Gestoppt.Tschö")
						.build();
			case "AMAZON.CancelIntent" :
				return AlexaSpeechletResponse.tell()
						.withText(
								"Abgebrochen. Tschüß.")
						.build();
			default : {
				log.error("Unknown intent: " + intentName);
				return AlexaSpeechletResponse.tell()
						.withText("Ich habe deine Absicht nicht verstanden")
						.build();
			}
		}
	}
	
	private SpeechletResponse handleCountBackwardIntent(Intent intent) {
		// Create the plain text output.

		Slot slot = intent.getSlot("Count");
		String value = slot.getValue();
		log.info("Slot value: " + value);
		if(value == null) {
			return AlexaSpeechletResponse.ask()
					.withRepromptText(REPROMPT_STANDARD)
					.withText("Konnte keine Zahl erkennen. Eine Zahl bitte.")
					.build();
			
		}
		Integer integerCount = null;
		try{
			integerCount = Integer.valueOf(value);
		}
		catch(NumberFormatException e)
		{
			return AlexaSpeechletResponse.ask()
			.withRepromptText(REPROMPT_STANDARD)
			.withText("Hab da keine Zahl erkannt. Nur " + value + " Gib mir nochmal ne Zahl bitte.")
			.build();
		}
		
		if(integerCount < 1)
		{
			return AlexaSpeechletResponse.ask()
			.withRepromptText(REPROMPT_STANDARD)
			.withText("Mit der Zahl " + integerCount + " macht rückwärts zählen doch keinen Spaß. Gib mir ne Zahl größer als 0")
			.build();
		}
		
		if(integerCount > 170)
		{
			return AlexaSpeechletResponse.ask()
			.withRepromptText(REPROMPT_STANDARD)
			.withSsml(SPEECHCON_HEILIGER_STROHSACK + " Bitte nur eine Zahl bis 170. Länger darf ich nicht reden und so lange hörst du sowieso nicht zu. Nenn mir nochmal ne Zahl bitte. ")
			.build();
		}
		
		StringBuilder backwardSpeech = new StringBuilder();
		while (integerCount >= 0)
		{
			backwardSpeech.append("<say-as interpret-as=\"cardinal\">" + integerCount+ "</say-as> ");
			integerCount --;
		}
		
		return AlexaSpeechletResponse.tell()
				.withSsml(backwardSpeech.toString())
				.build();
	}
	
	private SpeechletResponse handleHelp() {
		// Create the plain text output.

		return AlexaSpeechletResponse.ask()
				.withRepromptText(REPROMPT_STANDARD)
				.withText(SPEECH_HELP_TEXT)
				.withSimpleCardTitle("Hilfe")
				.withSimpleCardContent(SPEECH_HELP_TEXT).build();
	}


	@Override
	public void onSessionEnded(
			SpeechletRequestEnvelope<SessionEndedRequest> requestEnvelope) {
		log.debug("onSessionEnded requestId={}, sessionId={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
	}

}
